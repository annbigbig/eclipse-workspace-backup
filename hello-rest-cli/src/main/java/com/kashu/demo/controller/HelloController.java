package com.kashu.demo.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

	@RequestMapping("/greet")
	public String helloGreeting() {
		return "Hello World REST ";
	}
}