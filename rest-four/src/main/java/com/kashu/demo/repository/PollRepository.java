package com.kashu.demo.repository;

import org.springframework.data.repository.CrudRepository;
import com.kashu.demo.domain.Poll;

public interface PollRepository extends CrudRepository<Poll, Long> {

}
