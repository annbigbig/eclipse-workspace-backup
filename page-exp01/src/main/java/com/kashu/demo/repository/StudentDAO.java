package com.kashu.demo.repository;

import java.util.UUID;

import com.kashu.demo.entity.Student;

public interface StudentDAO extends GenericDAO<Student,UUID>{

}
