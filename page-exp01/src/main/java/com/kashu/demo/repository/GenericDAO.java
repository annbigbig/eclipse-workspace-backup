package com.kashu.demo.repository;

import java.io.Serializable;
import java.util.List;
import com.kashu.demo.dto.PagingParameters;

public interface GenericDAO<T,ID extends Serializable> {

	T findOne(ID id);
	
	List<T> findListByPagingParams(PagingParameters params);
	
	List<T> findAll();
	
	void save(T entity);
	
	T update(T entity);
	
	void delete(T entity);
	
	void deleteById(ID id);
}
