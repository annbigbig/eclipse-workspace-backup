package com.kashu.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kashu.demo.dto.PagingParameters;

@RestController("greetController")
@RequestMapping("/greet/")
public class GreetController {
	
	@RequestMapping(value="/hello", method=RequestMethod.GET)
	public String sayHello(PagingParameters params) {
		String result  = "field = " + params.getField()+ "\n";
		       result += "operator = " + params.getOperator() + "\n";
		       result += "value =" + params.getValue() + "\n";
		       result += "orderBy =" + params.getOrderBy() + "\n";
		       result += "orderType =" + params.getOrderType() + "\n";
		       result += "pageSize =" + params.getPageSize() + "\n";
		       result += "pageNumber =" + params.getPageNumber() + "\n";
		return result;
	}
}
