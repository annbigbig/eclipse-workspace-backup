package com.kashu.demo.repository;

import org.springframework.data.repository.CrudRepository;
import com.kashu.demo.domain.User;

public interface UserRepository extends CrudRepository<User, Long> {
	
	public User findByUsername(String username);
}

