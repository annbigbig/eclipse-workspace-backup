package com.kashu.demo.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.kashu.demo.domain.Poll;

public interface PollRepository extends PagingAndSortingRepository<Poll, Long> {

}
