package com.kashu.demo.repository;

import org.springframework.data.repository.CrudRepository;
import com.kashu.demo.domain.Option;

public interface OptionRepository extends CrudRepository<Option, Long> {

}