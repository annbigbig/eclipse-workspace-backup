package com.kashu.demo;

import java.util.ArrayList;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.google.common.collect.Lists;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static springfox.documentation.builders.PathSelectors.regex;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.builders.AuthorizationScopeBuilder;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.SecurityReference;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Bean
    public Docket v1ApiConfiguation() {
        return new Docket(DocumentationType.SWAGGER_2)
        		.groupName("QuickPoll v1 API")
                .select().apis(RequestHandlerSelectors.basePackage("com.kashu.demo.v1.controller"))
                .paths(regex("/v1/.*"))
                .build()
                .apiInfo(metaData())
                .useDefaultResponseMessages(false);
    }
	
	@Bean
    public Docket v2ApiConfiguation() {
        return new Docket(DocumentationType.SWAGGER_2)
        		.groupName("QuickPoll v2 API")
                .select().apis(RequestHandlerSelectors.basePackage("com.kashu.demo.v2.controller"))
                .paths(regex("/v2/.*"))
                .build()
                .apiInfo(metaData())
                .useDefaultResponseMessages(false);
    }
    
	@Bean
    public Docket v3ApiConfiguation() {
		AuthorizationScope[] authScopes = new AuthorizationScope[1];
	    authScopes[0] = new AuthorizationScopeBuilder()
	            .scope("read")
	            .description("read access")
	            .build();
	    SecurityReference securityReference = SecurityReference.builder()
	            .reference("test")
	            .scopes(authScopes)
	            .build();
	    ArrayList<SecurityContext> securityContexts = Lists.newArrayList(
	            SecurityContext.builder()
	                    .securityReferences(Lists.newArrayList(securityReference))
	                    .build()
	    );
        return new Docket(DocumentationType.SWAGGER_2)
        		.groupName("QuickPoll v3 API")
        		.securitySchemes(Lists.newArrayList(new BasicAuth("test")))
                .securityContexts(securityContexts)
                .select().apis(RequestHandlerSelectors.basePackage("com.kashu.demo.v3.controller"))
                .paths(regex("/v3/.*"))
                .build()
                .apiInfo(metaData())
                .useDefaultResponseMessages(false);
    }
	
	private ApiInfo metaData() {
        return new ApiInfoBuilder()
                .title("Quick Poll REST API")
                .description("\"Spring Boot REST API for Quick Poll Application\"")
                .version("0.0.2")
                .license("Apache License Version 2.0")
                .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0\"")
                .contact(new Contact("Kun-An Hsu", "https://www.dq5rocks.com/wordpress/", "annbigbig@gmail.com"))
                .build();
    }
}
