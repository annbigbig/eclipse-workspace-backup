package com.kashu.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static springfox.documentation.builders.PathSelectors.regex;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.builders.ApiInfoBuilder;


@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Bean
    public Docket quickPollApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select().apis(RequestHandlerSelectors.basePackage("com.kashu.demo.controller"))
                .paths(regex("/.*"))
                .build()
                .apiInfo(metaData())
                .useDefaultResponseMessages(false);
    }
	
	private ApiInfo metaData() {
        return new ApiInfoBuilder()
                .title("Quick Poll REST API")
                .description("\"Spring Boot REST API for Quick Poll Application\"")
                .version("0.0.2")
                .license("Apache License Version 2.0")
                .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0\"")
                .contact(new Contact("Kun-An Hsu", "https://www.dq5rocks.com/wordpress/", "annbigbig@gmail.com"))
                .build();
    }
}
