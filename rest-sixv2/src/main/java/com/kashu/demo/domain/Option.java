package com.kashu.demo.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import io.swagger.annotations.ApiModelProperty;

@Entity
public class Option {

    @Id
    @GeneratedValue
    @Column(name="OPTION_ID")
    @ApiModelProperty(notes = "The database generated Option ID")
	private Long id;
    
    @Column(name="OPTION_VALUE")
    @ApiModelProperty(notes = "The name of this option", required=true)
	private String value;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return getId() + "," + getValue();
	}
}