package com.kashu.demo.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import io.swagger.annotations.ApiModelProperty;

@Entity
public class Vote {

	@Id
	@GeneratedValue
	@Column(name="VOTE_ID")
	@ApiModelProperty(notes = "The database generated vote id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="OPTION_ID")
	@ApiModelProperty(notes = "option id that user cast for a poll", required=true)
	private Option option;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Option getOption() {
		return option;
	}
	public void setOption(Option option) {
		this.option = option;
	}
	
	@Override
	public String toString() {
		return getId() + ", " + getOption();
	}
	
}