package com.kashu.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kashu.demo.domain.Vote;
import com.kashu.demo.dto.OptionCount;
import com.kashu.demo.dto.VoteResult;
import com.kashu.demo.repository.VoteRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "computeresult", description = "Compute Results API")
public class ComputeResultController {
	
	@Autowired
	private VoteRepository voteRepository;

	@RequestMapping(value="/computeresult", method=RequestMethod.GET, produces="application/json")
	@ApiOperation(value = "Computes the results of a given Poll", response = VoteResult.class)
	public ResponseEntity<?> computeResult(@RequestParam Long pollId) {
		VoteResult voteResult = new VoteResult();
		Iterable<Vote> allVotes = voteRepository.findByPoll(pollId);
		
		// Algorithm to count votes
		int totalVotes = 0;
		Map<Long, OptionCount> tempMap = new HashMap<Long, OptionCount>();
		for(Vote v : allVotes) {
			totalVotes ++;
			// Get the OptionCount corresponding to this Option
			OptionCount optionCount = tempMap.get(v.getOption().getId());
			if(optionCount == null) {
				optionCount = new OptionCount();
				optionCount.setOptionId(v.getOption().getId());
				tempMap.put(v.getOption().getId(), optionCount);
			}
			optionCount.setCount(optionCount.getCount()+1);
		}
		
		voteResult.setTotalVotes(totalVotes);
		voteResult.setResults(tempMap.values());
		
		return new ResponseEntity<VoteResult>(voteResult, HttpStatus.OK);
	}
	
}

